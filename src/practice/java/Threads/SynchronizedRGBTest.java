package practice.java.Threads;

class SynchronizedRGBTest {
    public static void main(String[] args) {
        SynchronizedRGB color =
                new SynchronizedRGB(0, 0, 0, "Pitch Black");

        //not synchronized
        int myColorInt = color.getRGB();      //Statement 1
        String myColorName = color.getName(); //Statement 2

        //synchronized
        synchronized (color)
        {
            myColorInt = color.getRGB();
            myColorName = color.getName();
        }
    }
}