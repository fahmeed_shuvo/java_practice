package practice.java;

public class bicycle
{
    int speed = 0;
    int gear = 0;

    void setGear(int value)
    {
        gear = value;
    }

    void setSpeed(int increment)
    {
        speed += increment;
    }

    void brakes(int decrement)
    {
        speed -= decrement;
    }

    void printstates()
    {
        System.out.println("Gear: " + gear + "; Speed: " + speed);
    }
}
