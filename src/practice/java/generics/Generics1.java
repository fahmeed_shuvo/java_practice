package practice.java.generics;

public class Generics1<K>{
    private K key;

    Generics1(K key)
    {
        this.key = key;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    void operationKey()
    {
        if (getKey() instanceof String)
        {
            System.out.println("Key is a string.");
        }
        else if (getKey() instanceof Integer)
        {
            System.out.println("Key is an integer.");
        }
    }
}