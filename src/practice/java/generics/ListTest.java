package practice.java.generics;

import java.util.*;

public class ListTest
{
    public static void main(String[] args)
    {
        String[] s1 = new String[]{"I", "came", "I", "saw", "I", "left"};
        /*List<String> list = new ArrayList<String>();

        for (String a : s1)
            list.add(a);

        Collections.shuffle(list, new Random());*/

        List<String> list = Arrays.asList(s1);
        List<String> list2 = new ArrayList<>(list);
        Collections.shuffle(list);

        System.out.println(list);

        boolean bool = list2.remove("le");
        System.out.println(list2.toString());
        System.out.println(bool);

    }
}
