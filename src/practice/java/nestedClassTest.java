package practice.java;

public class nestedClassTest
{
    public static void main(String[] args)
    {
        nestedClass nc1 = new nestedClass();
        nestedClass.NestedStatic ns1 = new nestedClass.NestedStatic();
        System.out.println(ns1.multiplyWithNested());
        nestedClass.InnerClass ic1 = nc1.new InnerClass();
        System.out.println(ic1.innerMultiply());
        System.out.println(nc1.setVar4(16));
    }
}