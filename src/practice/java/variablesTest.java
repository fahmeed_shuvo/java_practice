package practice.java;

public class variablesTest
{
    public static void main(String[] args)
    {
        byte b1 = 127;
        short s1 = 32_767;
        long l1 = 65641651L;
        float f1 = 1.2345f;
        double d1 = 1.23456d;
        double d2 = 1.2345e2;
        char c1 = 'j';
        String st1 = "java_learning";

        System.out.println("Byte " + b1);
        System.out.println("Short " + s1);
        System.out.println("Long " + l1);
        System.out.println("Float " + f1);
        System.out.println("Double 1st " + d1);
        System.out.println("Double 2nd " + d2);
        System.out.println("Char " + c1);
        System.out.println("String " + st1);
    }
}
