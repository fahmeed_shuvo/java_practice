package practice.java;

import java.time.LocalDate;
import java.time.Period;

public class anonymous
{
    public interface Human
    {
        public String getSex();
        public int getAge();
        public String getName();
    }

    public class EnglishMan implements Human
    {
        private String sex = null;
        private String name = null;
        private LocalDate birthDate = null;

        EnglishMan (String newName, String newSex, LocalDate newBirthDate)
        {
            name = newName;
            sex = newSex;
            birthDate = newBirthDate;
        }
        public void setSex(String newSex)
        {
            sex = newSex;
        }
        public void setName(String newName)
        {
            name = newName;
        }
        public void setBirthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
        }

        public String getSex() {
            return sex;
        }
        public String getName() {
            return name;
        }
        public int getAge() {
            LocalDate now = LocalDate.now();
            Period diff = Period.between(birthDate,now);
            return diff.getYears();
        }
    }

    public static void main(String[] args)
    {
        anonymous ano = new anonymous();
        EnglishMan eman1 = ano.new EnglishMan("John","Male", LocalDate.of(1988,12,25));

        System.out.println("1st man is " + eman1.getName() + " and is a " + eman1.getSex());
        System.out.println("He is " + eman1.getAge() + " years old.");

        Human bengali = new Human() {
            public String getSex() {
                return "Male";
            }

            public int getAge() {
                return 28;
            }

            public String getName() {
                return "Shuvo";
            }
        };

        System.out.println("2nd man is " + bengali.getName() + " and is a " + bengali.getSex());
        System.out.println("He is " + bengali.getAge() + " years old.");

        Human human2 = new Human() {

            public String getSex() {
                return null;
            }

            public int getAge() {
                return 0;
            }

            public String getName() {
                return null;
            }
        };
    }
}
