package practice.java.producerconsumerproblem;

import java.util.Random;

public class WaiterClass implements Waiter{
    private String type;
    ConveyorBelt belt;

    public WaiterClass(String type, ConveyorBelt belt) {
        this.type = type;
        this.belt = belt;
    }

    public String getType() {
        return type;
    }

    @Override
    public void run() {
        while (true)
        {
            synchronized (this) {
                try {
                    while (!belt.isNotFull()) {
                        try {
                            System.out.println(type + " waiter is waiting...");
                            this.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    Thread.sleep(1000);
                    if (pop()) {
//                        Thread.sleep(5000);
                        this.notifyAll();
                    }
                } catch (InterruptedException | IllegalMonitorStateException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized boolean pop()
    {
        if (!belt.getQueue().isEmpty())
        {
            if (belt.getQueue().peek().equals(type) && new Random().nextBoolean())
            {
                System.out.println("Took a " + type + " cake by " + type +  " waiter.");
                belt.pop();
                return true;
            }
        }
        System.out.println(type + " waiter is idle...");
        return false;
    }
}
