package practice.java.producerconsumerproblem;

import java.util.Random;

public class Chef implements Runnable{
    private String type;
    private int beltCap;
    ConveyorBelt belt;

    public Chef(String type, ConveyorBelt belt, int beltCap) {
        this.type = type;
        this.belt = belt;
        this.beltCap = beltCap;
    }

    public int getBeltCap() {
        return beltCap;
    }
    public String getType() {
        return type;
    }
    public ConveyorBelt getBelt() {
        return belt;
    }

    @Override
    public void run() {
        while (true)
        {
            synchronized (this) {
                while (belt.getCurrentSize() == beltCap) {
                    try {
                        System.out.println(type + " chef is waiting...");
                        this.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    Thread.sleep(1000);
                    if (push())
                    {
                        this.notifyAll();
                    }
                } catch (InterruptedException | IllegalMonitorStateException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized boolean push()
    {
        if (new Random().nextBoolean())
        {
            if (belt.isNotFull())
            {
                System.out.println("Produced a " + type + " cake by " + type +  " chef.");
                belt.put(type);
                return true;
            }
            System.out.println(type + " chef is idle...");
        }
        return false;
    }
}