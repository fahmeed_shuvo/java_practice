package practice.java.producerconsumerproblem;

public interface Waiter extends Runnable {
    public boolean pop();
}
