package practice.java.producerconsumerproblem;

public class MainApp {

    public static void main(String[] args) throws IllegalThreadStateException{
        int beltCapacity = 5;
        //conveyor belts
        ConveyorBelt beltVanilla = new ConveyorBelt("vanilla", beltCapacity);
        ConveyorBelt beltChocolate = new ConveyorBelt("chocolate", beltCapacity);
        ConveyorBelt beltFinal = new ConveyorBelt("final", beltCapacity);
        ConveyorBelt[] inputBelts = {beltVanilla, beltChocolate};
        ConveyorBelt[] allBelts = {beltVanilla, beltChocolate, beltFinal};

        //chefs
        Chef vanillaChef = new Chef("vanilla", beltVanilla, beltCapacity);
        Chef chocolateChef = new Chef("chocolate", beltChocolate, beltCapacity);
        DesignerChef designerChef = new DesignerChef("designer", beltFinal, beltCapacity, inputBelts);

        //waiters
        Waiter vanillaWaiterClass = new WaiterClass("vanilla", beltFinal);
        Waiter chocolateWaiterClass = new WaiterClass("chocolate", beltFinal);
        Waiter waiter1 = new FreeWaiterClass("waiter 1", beltFinal);
        Waiter waiter2 = new FreeWaiterClass("waiter 2", beltFinal);
        Waiter waiter3 = new FreeWaiterClass("waiter 3", beltFinal);

        Thread t1 = new Thread(vanillaChef);
        Thread t2 = new Thread(chocolateChef);
        Thread t3 = new Thread(designerChef);
//        Thread t4 = new Thread(vanillaWaiter);
//        Thread t5 = new Thread(chocolateWaiter);
        Thread t4 = new Thread(waiter1);
        Thread t5 = new Thread(waiter2);
        Thread t6 = new Thread(waiter3);


        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();

        new Thread()
        {
            @Override
            public void run() {
                while (true)
                {
                    try {
                        Thread. sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    for (ConveyorBelt belt : allBelts) {
                        belt.printQueue();
                    }
                }
            }
        }.start();

//        for (int i = 0; i < 6; i++) {
//            vanillaChef.push();
//        }
//        beltVanilla.printQueue();
//        chocolateChef.push();
//        designerChef.pop();
//        designerChef.push();
//        beltFinal.printQueue();
//        vanillaWaiter.pop();
//        chocolateWaiter.pop();
//        for (ConveyorBelt belt : allBelts) {
//            belt.printQueue();
//        }
    }

}
