package practice.java.producerconsumerproblem;

import java.util.Random;

public class FreeWaiterClass extends WaiterClass {
    public FreeWaiterClass(String type, ConveyorBelt belt) {
        super(type, belt);
    }

    @Override
    public synchronized boolean pop() {
        if (!belt.getQueue().isEmpty() && new Random().nextBoolean())
        {
            try {
                System.out.println("Took a " + belt.pop() + " cake by " + getType());
                return true;
            }
            catch (NullPointerException ignored){}
        }
        System.out.println(getType() + " waiter is idle...");
        return false;
    }
}
