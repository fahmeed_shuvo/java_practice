package practice.java.producerconsumerproblem;

import java.util.Random;

public class DesignerChef extends Chef{
    ConveyorBelt[] belts;
    String cake;

    public DesignerChef(String name, ConveyorBelt belt, int beltSize, ConveyorBelt[] belts) {
        super(name, belt, beltSize);
        this.belts = belts;
    }

    @Override
    public void run() {
        while (true)
        {
            synchronized (this) {
                while (getBelt().getCurrentSize() == getBeltCap()) {
                    try {
                        System.out.println(getType() + " chef is waiting...");
                        this.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    Thread.sleep(1000);
                    if (pop()) {
                        push();
                        this.notifyAll();
                    }
                } catch (InterruptedException | IllegalMonitorStateException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public synchronized boolean push() {
        if (belt.isNotFull())
        {
            System.out.println("Designed a " + cake + " cake by " + getType() +  " chef.");
            belt.put(cake);
            return true;
        }
        System.out.println(getType() + " chef is idle...");
        return false;
    }

    public synchronized boolean pop()
    {
        int random = new Random().nextInt(belts.length);
        cake = belts[random].pop();
        if (cake != null)
        {
            System.out.println("Took a " + cake + " cake by designer chef.");
            return true;
        }
        return false;
    }
}
