package practice.java.locktest;

public class Independent implements Runnable {
    @Override
    public void run() {
        synchronized (Main.lock)
        {
            System.out.println("independent is running.");
            Main.lock.notifyAll();
        }
    }
}
