package practice.java.locktest;

public class Dependent implements Runnable {
    @Override
    public void run() {
        synchronized (Main.lock)
        {
            try {
                System.out.println("dependent is waiting..." + this);
                Main.lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Dependent is running");
    }
}
