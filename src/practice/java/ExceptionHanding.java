package practice.java;

public class ExceptionHanding
{
    public static void main(String[] args)
    {
        int i=5;
        int j = 2;
        int[] div = new int[1];
        try{
            div[2] = i/j;
        }
        catch(ArithmeticException AE)
        {
            System.out.println(AE.getMessage() + " exception found.");
        }
        catch (ArrayIndexOutOfBoundsException AI)
        {
            System.out.println(AI + " exceptions found.");
        }
        System.out.println(div[0]);
    }
}
