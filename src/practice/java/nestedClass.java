package practice.java;

public class nestedClass
{
    private int intVar1 = 0;
    public int intVar2 = 1;
    int intVar3 = 2;
    static int intVar4 = 3;

    public static class NestedStatic
    {
        public int insideNested = 1;

        public int multiplyWithNested()
        {
            return insideNested* nestedClass.intVar4;
        }
    }

    public class InnerClass
    {
        public int innerInt = 4;

        public int innerMultiply()
        {
            return innerInt*intVar3;
        }
    }

    public int setVar4(int val)
    {
        intVar4 = val;
        return intVar4;
    }
}
