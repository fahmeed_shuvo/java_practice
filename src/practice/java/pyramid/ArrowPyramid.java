package practice.java.pyramid;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ArrowPyramid implements Runnable {
    private int column;
    private static int pyramidRow = 1;
    Map<Integer, String> syncMap = Collections.synchronizedMap(new HashMap<>());

    ArrowPyramid(int column) {
        this.column = column;
    }

    @Override
    public void run() {
        linePrint(lastInt(Thread.currentThread().getName()), pyramidRow++);
    }

    public void linePrint(int threadNumber, int row)
    {
        int skip = column / 2 + 1;
        int digitsInLine = row * 2 - 1;
        String line = "";
        if(row <= skip)
        {
            line += loop(skip - row); //prints spaces per line
            line += loop(threadNumber < 10 ? digitsInLine : digitsInLine / 2,
                    threadNumber); //prints numbers as a string
        }
        else
        {
            line += loop(threadNumber < 10 ? column + 1 : skip - 1, threadNumber);
        }
        syncMap.put(row,line);
    }

    public int lastInt(String str)
    {
        String[] parts = str.split("-");
        String lastDigit = parts[parts.length - 1].trim();
        return Integer.parseInt(lastDigit);
    }

    public void printPyramid()
    {
        for (int i = 1; i <= syncMap.size(); i++) {
            System.out.println(syncMap.get(i));
        }
    }

    public String loop(int loopCount)
    {
        String line = "";
        for (int j = 0; j < loopCount; j++) {
            line += " ";
        }
        return line;
    }
    public String loop(int loopCount, int number)
    {
        String line = "";
        for (int j = 0; j < loopCount; j++) {
            line += number;
        }
        if (number > 9)
        {
            line += number / 10;
        }
        return line;
    }
}