package practice.java.pyramid;

public interface Pyramid {
    public void draw(int n, int m);
}
