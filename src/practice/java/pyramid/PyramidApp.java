package practice.java.pyramid;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PyramidApp {
    public static void main(String[] args) {
        int threadPool = 10;
        int row = 20;
        int column = 20;

        ArrowPyramid pyramid = new ArrowPyramid(column);

        ExecutorService service = Executors.newFixedThreadPool(threadPool);
        for (int counter = 1; counter <= row; counter++) {
            Thread t = new Thread(pyramid);
            service.execute(t);
        }
        service.shutdown();

        pyramid.printPyramid();
    }
}
