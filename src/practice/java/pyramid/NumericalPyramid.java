package practice.java.pyramid;

public class NumericalPyramid// implements Pyramid
{
    public void draw(int rows) {
        int i, j, k, columns;
        columns = 10;
        for (i = 1; i <= rows; i++) {
            k = Math.min(i, columns);
            for (j = Math.min(rows, columns); j > k; j--) {
                System.out.print(" ");
            }
            for (j = 1; j < Math.min(i, columns * 2); j++) {
                System.out.print(j);
            }
            for (j = k; j > 0; j--)
//                for(j=Math.min(i,columns*2);j>0;j--)
            {
                System.out.print(j);
            }
            System.out.println();
        }
    }
}
