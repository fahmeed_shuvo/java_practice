package practice.java;

import java.util.Scanner;

public class calculator
{
    public static void main(String[] args)
    {
        float number1, number2;
        char symbol;
        Scanner input = new Scanner(System.in);

        System.out.println("Insert 1st number:");
        number1 = input.nextFloat();
        System.out.println("Insert 2nd number:");
        number2 = input.nextFloat();
        System.out.println("Insert symbol(+,-,*,/):");
        symbol = input.next().charAt(0);
        if (symbol == '+')
        {
            System.out.println("The sum is: " + (number1+number2));
        }
        else if (symbol == '-')
        {
            System.out.println("The difference is: " + (number1-number2));
        }
        else if (symbol == '*')
        {
            System.out.println("The multiplication is: " + (number1*number2));
        }
        else if (symbol == '/')
        {
            if (number2 != 0)
            {
                System.out.println("The division is: " + (number1/number2));
            }
            else
            {
                System.out.println("The divisor can't be empty.");
            }
        }
        else
        {
            System.out.println("Invalid input.");
        }
    }
}
